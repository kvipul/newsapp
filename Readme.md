#Pseudo News App
Development Time: 7 Hrs

##Aim
Develop a pseudo Mobile application(Android) which should list and open best outlinked news articles from the app.

Your application should fetch all news article details from the provided API and list them on the home screen. These details include Headlines and URLs among other variables. When user clicks on the listed headline, a browser window should load and display the article.

##App Features
- Home screen displays news results from the api with along filter and sort options
- By clicking news card user can get more info about the news in app itself as well as in mobile browser
- Animation is also used in most of the android events
- User can bookmark any news card for later use
- GenericRequest is used to cache the api response for few day, with this app will work in offline mode also
- Code has been written in separate modules to remove code duplication, maintain better code structure and to make app easily scalable
- Proguard is also used for release build to make app size small and to make app more secure

##Third Party libraries and References
- Volley: For Network Call
- Data Source: [Inshorts Outlinks API](http://starlord.hackerearth.com/newsjson)
- Android Support Library
- Some Files like GenericRequest.java, SessionManager.java, AppController.java are the files that I've been using for a very long time. Some section of these files may be from internet 
- Icon image source: Google image