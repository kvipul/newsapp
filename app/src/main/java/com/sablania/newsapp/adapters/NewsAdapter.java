package com.sablania.newsapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sablania.newsapp.BookmarkActivity;
import com.sablania.newsapp.BrowserActivity;
import com.sablania.newsapp.R;
import com.sablania.newsapp.models.NewsModel;
import com.sablania.newsapp.utils.SessionManagerV2;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    Context context;
    LayoutInflater inflater;
    List<NewsModel> list;
    SessionManagerV2 sessionManagerV2;
    boolean inBookmarkActivity;

    public NewsAdapter(Context context, List<NewsModel> list, boolean inBookmarkActivity) {
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sessionManagerV2 = new SessionManagerV2(context);
        this.inBookmarkActivity = inBookmarkActivity;

    }

    @Override
    public NewsAdapter.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View convertView = inflater.inflate(R.layout.news_card_item, parent, false);
        NewsAdapter.ViewHolder holder = new NewsAdapter.ViewHolder(convertView);

        return holder;
    }

    @Override
    public void onBindViewHolder(final NewsAdapter.ViewHolder holder, final int position) {
        final NewsModel newsModel = list.get(position);
        holder.tvTitle.setText("#"+(position+1) + "  " + newsModel.getTitle());
        holder.tvPublisher.setText("By "+ newsModel.getPublisher()+" ("+newsModel.getHostname()+")");
        holder.tvCategory.setText(newsModel.getCategoryName());

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss d MMM, yyyy");
        holder.tvTime.setText(sdf.format(new Date(newsModel.getTime())));

        if (sessionManagerV2.isBookmarked(newsModel)) {
            holder.cbBookmark.setChecked(true);
        } else {
            holder.cbBookmark.setChecked(false);

        }
        

        holder.cbBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.cbBookmark.isChecked()) {
                    sessionManagerV2.addBookmarkedNews(newsModel);
                } else {
                    sessionManagerV2.removeBookmarkedNews(newsModel);
                    if(inBookmarkActivity){
                        list.remove(position);
                        notifyDataSetChanged();
                        if(list.isEmpty())
                        ((BookmarkActivity)context).recreate();
                    }
                }
            }
        });
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, BrowserActivity.class);
                intent.putExtra("model", newsModel);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvTitle, tvPublisher, tvCategory, tvTime;
        AppCompatCheckBox cbBookmark;
        LinearLayout view;

        ViewHolder(View itemView) {
            super(itemView);

            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvPublisher = (TextView) itemView.findViewById(R.id.tv_publisher);
            tvCategory = (TextView) itemView.findViewById(R.id.tv_category);
            tvTime = (TextView) itemView.findViewById(R.id.tv_time);
            cbBookmark = (AppCompatCheckBox) itemView.findViewById(R.id.cb_bookmark);
            view = (LinearLayout) itemView.findViewById(R.id.whole_view);

        }
    }
}
