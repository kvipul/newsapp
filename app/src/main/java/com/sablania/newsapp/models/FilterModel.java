package com.sablania.newsapp.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ViPul Sublaniya on 17-09-2017.
 */

public class FilterModel implements Serializable {
    List<String> categories;

    String category;
    String sortBy;

    public List<String> getCategories() {
        if(categories!=null) {
            return categories;
        }
        return new ArrayList<>();

    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

}
