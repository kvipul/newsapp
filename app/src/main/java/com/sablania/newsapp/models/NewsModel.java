package com.sablania.newsapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NewsModel implements Serializable {

    @SerializedName("ID")
    long id;

    @SerializedName("TITLE")
    String title;

    @SerializedName("URL")
    String url;

    @SerializedName("PUBLISHER")
    String publisher;

    @SerializedName("CATEGORY")
    String category;

    @SerializedName("HOSTNAME")
    String hostname;

    @SerializedName("TIMESTAMP")
    long time;

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getPublisher() {
        return publisher;
    }

    public String getCategory() {
        return category;
    }

    public String getHostname() {
        return hostname;
    }

    public long getTime() {
        return time;
    }

    public String getCategoryName() {
        if(getCategory().equalsIgnoreCase("b")) {
            return  "Business";
        }else if(getCategory().equalsIgnoreCase("t")) {
            return "Science and Technology";
        }else if(getCategory().equalsIgnoreCase("e")) {
            return "Entertainment";
        } else if (getCategory().equalsIgnoreCase("m")) {
            return "Health";
        }
        return "No Defined Category";
    }
}
