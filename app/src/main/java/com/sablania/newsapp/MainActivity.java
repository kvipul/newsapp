package com.sablania.newsapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sablania.newsapp.adapters.NewsAdapter;
import com.sablania.newsapp.models.FilterModel;
import com.sablania.newsapp.models.NewsModel;
import com.sablania.newsapp.utils.AppController;
import com.sablania.newsapp.utils.CompareNewsObject;
import com.sablania.newsapp.utils.Constants;
import com.sablania.newsapp.utils.GenericRequest;
import com.sablania.newsapp.utils.SessionManagerV2;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MainActivity extends AppCompatActivity {
    SessionManagerV2 sessionManagerV2;
    RecyclerView rvNews;
    TextView noData;
    Button tryAgain;
    MenuItem filterOption;

    NewsAdapter newsAdapter;

    List<NewsModel> dataList;
    String API = "http://starlord.hackerearth.com/newsjson";
    boolean cacheUsed;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initialize();
        fetchNews();

        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchNews();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        filterOption = menu.findItem(R.id.action_add_filters);
        setVisibilityOfFilterOptions();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(this, "No setting added!", Toast.LENGTH_SHORT).show();
        }else if (id==R.id.action_bookmarks) {
            Intent intent = new Intent(MainActivity.this, BookmarkActivity.class);
            startActivity(intent);
        } else if (id == R.id.action_add_filters) {
            Intent intent = new Intent(MainActivity.this, FiltersActivity.class);
            startActivityForResult(intent, 101);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            List<NewsModel> newList = new ArrayList<>();
            FilterModel filterModel = sessionManagerV2.getFilterModel();

            //filter list by category
            if (filterModel.getCategory().equalsIgnoreCase("All")) {
                newList.addAll(dataList);
            }else {
                for (NewsModel n : dataList) {
                    if (n.getCategoryName().equalsIgnoreCase(filterModel.getCategory())) {
                        newList.add(n);
                    }
                }
            }

            //sort filterd list
            Collections.sort(newList, new CompareNewsObject(filterModel.getSortBy().equalsIgnoreCase("Newest First")));

            //set adapter
            newsAdapter = new NewsAdapter(MainActivity.this, newList, false);
            rvNews.setAdapter(newsAdapter);
            rvNews.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(newsAdapter!=null)
            newsAdapter.notifyDataSetChanged();
    }

    private void initialize() {
        rvNews = (RecyclerView) findViewById(R.id.rv_news);
        noData = (TextView) findViewById(R.id.tv_no_data);
        tryAgain = (Button) findViewById(R.id.try_again);
        sessionManagerV2 =  new SessionManagerV2(MainActivity.this);
    }

    private void fetchNews() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading data...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        cacheUsed = false;

        //custom generic request for caching data
        @SuppressWarnings("unchecked")
        GenericRequest genericRequest = new GenericRequest(Request.Method.GET,
                API, String.class, null, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                cacheUsed = true;
                Type listType = new TypeToken<List<NewsModel>>() {
                }.getType();

                dataList = new Gson().fromJson(response.toString(), listType);
                newsAdapter = new NewsAdapter(MainActivity.this, dataList, false);
                rvNews.setAdapter(newsAdapter);
                rvNews.setLayoutManager(new LinearLayoutManager(MainActivity.this));

                if (dataList.isEmpty()) {
                    //if list is empty
                    noData.setVisibility(View.VISIBLE);
                } else {
                    noData.setVisibility(View.GONE);
                    Set<String> set = new HashSet<>();
                    for (NewsModel n : dataList) {
                        set.add(n.getCategoryName());
                    }
                    FilterModel filterModel = sessionManagerV2.getFilterModel();
                    List<String> categories = new ArrayList<String>();
                    categories.add("All");
                    categories.addAll(set);
                    filterModel.setCategories(categories);
                    sessionManagerV2.setFilterModel(filterModel);
                }
                progressDialog.dismiss();
                setVisibilityOfFilterOptions();

                tryAgain.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.handleVolleyError(MainActivity.this, error, true);
                progressDialog.dismiss();
                //hide filter option if no data is loaded in recycler view
                setVisibilityOfFilterOptions();
                if(!cacheUsed){
                    //if no internet
                    tryAgain.setVisibility(View.VISIBLE);
                }
            }
        }, Constants.getHeader(), false);

        AppController.getInstance(this).addToRequestQueue(genericRequest, "news");
    }

    private void setVisibilityOfFilterOptions() {
        if (filterOption == null) {
            return;
        }
        if (newsAdapter != null && dataList.size() != 0) {
            filterOption.setVisible(true);
        } else {
            filterOption.setVisible(false);
        }
    }

}
