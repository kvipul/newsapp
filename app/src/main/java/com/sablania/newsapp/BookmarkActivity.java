package com.sablania.newsapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.sablania.newsapp.adapters.NewsAdapter;
import com.sablania.newsapp.models.NewsModel;
import com.sablania.newsapp.utils.SessionManagerV2;

import java.util.List;

public class BookmarkActivity extends AppCompatActivity {
    SessionManagerV2 sessionManagerV2;
    RecyclerView rvNews;
    TextView noData;
    NewsAdapter newsAdapter;

    List<NewsModel> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Bookmarks");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rvNews = (RecyclerView) findViewById(R.id.rv_news);
        noData = (TextView) findViewById(R.id.tv_no_data);

        sessionManagerV2 = new SessionManagerV2(this);
        list = sessionManagerV2.getBookmarkedNews();

        newsAdapter = new NewsAdapter(BookmarkActivity.this, list, true);
        rvNews.setAdapter(newsAdapter);
        rvNews.setLayoutManager(new LinearLayoutManager(BookmarkActivity.this));

        if (list.isEmpty()) {
            noData.setVisibility(View.VISIBLE);
        } else {
            noData.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
