package com.sablania.newsapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import com.sablania.newsapp.models.NewsModel;

public class BrowserActivity extends AppCompatActivity {
    WebView webView;
    CardView cvOpenInBrowser;
    NewsModel newsModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);

        getSupportActionBar().setTitle("News Detail");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cvOpenInBrowser = (CardView) findViewById(R.id.cv_open_in_browser);
        webView = (WebView) findViewById(R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);//enable javascript, default if false

        newsModel = (NewsModel) getIntent().getSerializableExtra("model");
        webView.loadUrl(newsModel.getUrl());
        Toast.makeText(this, "Be Patient! This might take a while...", Toast.LENGTH_LONG).show();
        cvOpenInBrowser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open link in mobile's default browser
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(newsModel.getUrl()));
                startActivity(browserIntent);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
