package com.sablania.newsapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.sablania.newsapp.models.FilterModel;
import com.sablania.newsapp.utils.SessionManagerV2;

import java.util.ArrayList;

public class FiltersActivity extends AppCompatActivity {

    Spinner spnSortOptions, spnCategory;
    Button btnApply;
    SessionManagerV2 sessionManagerV2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filters);

        getSupportActionBar().setTitle("Choose Filters");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        spnSortOptions = (Spinner) findViewById(R.id.spn_sort_option);
        spnCategory = (Spinner) findViewById(R.id.spn_category);
        btnApply = (Button) findViewById(R.id.btn_save);
        sessionManagerV2 = new SessionManagerV2(this);

        //setup sort option spinner=============================================================
        ArrayList<String> items = new ArrayList<>();
        items.add("Newest First");
        items.add("Oldest First");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spnSortOptions.setAdapter(dataAdapter);
        spnSortOptions.setSelection(0);

        //setup sort option spinner=============================================================
        final FilterModel filterModel = sessionManagerV2.getFilterModel();

        ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, filterModel.getCategories());
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spnCategory.setAdapter(categoryAdapter);
        spnCategory.setSelection(0);

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String category = spnCategory.getSelectedItem().toString();
                filterModel.setCategory(category);
                filterModel.setSortBy(spnSortOptions.getSelectedItem().toString());
                sessionManagerV2.setFilterModel(filterModel);
                setResult(RESULT_OK);
                finish();

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        } else if (id == R.id.action_reset) {
            //reset value of all the filter options
            spnCategory.setSelection(0);
            spnSortOptions.setSelection(0);
            Toast.makeText(this, "Filter Reset!", Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reset, menu);
        return true;
    }
}
