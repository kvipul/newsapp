package com.sablania.newsapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sablania.newsapp.models.FilterModel;
import com.sablania.newsapp.models.NewsModel;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class SessionManagerV2 {

    // Sharedpref file name
    private static final String PREF_NAME = "SABLANIA_NEWS";
    private static final String KEY_FILTER = "FILTER";
    private static final String KEY_BOOKMARKS = "BOOKMARKS";
    private SharedPreferences pref;
    // Editor for Shared preferences
    private SharedPreferences.Editor editor;

    // Context
    private Context context;
    private int PRIVATE_MODE = 0;
    Gson gson;

    public SessionManagerV2(Context context) {
        this.context = context;
        pref = this.context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        gson = new Gson();
    }

    public FilterModel getFilterModel() {
        String filterModelStr = pref.getString(KEY_FILTER, "");
        if(filterModelStr.isEmpty()){
            return new FilterModel();
        }
        FilterModel filterModel = gson.fromJson(filterModelStr, FilterModel.class);

        return filterModel;
    }

    public void setFilterModel(FilterModel filterModel) {
        String filterModelJson = gson.toJson(filterModel);

        editor.putString(KEY_FILTER, filterModelJson);
        editor.commit();
    }

    public ArrayList<NewsModel> getBookmarkedNews() {
        String str = pref.getString(KEY_BOOKMARKS, "");
        if(str.isEmpty()){
            return new ArrayList<>();
        }
        Type listType = new TypeToken<ArrayList<NewsModel>>() {
        }.getType();
        return gson.fromJson(str, listType);
    }

    public void setBookmarkedNews(ArrayList<NewsModel> bookmarkedNews) {
        String string = gson.toJson(bookmarkedNews);

        editor.putString(KEY_BOOKMARKS, string);
        editor.commit();
    }

    public void addBookmarkedNews(NewsModel newsModel) {
        ArrayList<NewsModel> bookmarkedNews = getBookmarkedNews();
        bookmarkedNews.add(newsModel);
        setBookmarkedNews(bookmarkedNews);
        Toast.makeText(context, "Added to Bookmarks", Toast.LENGTH_SHORT).show();
    }

    public void removeBookmarkedNews(NewsModel newsModel) {
        ArrayList<NewsModel> bookmarkedNews = getBookmarkedNews();
        for(int i=0; i<bookmarkedNews.size();i++){
            if(bookmarkedNews.get(i).getId()==newsModel.getId()){
                bookmarkedNews.remove(i);
                break;
            }
        }
        setBookmarkedNews(bookmarkedNews);
        Toast.makeText(context, "Removed from Bookmarks", Toast.LENGTH_SHORT).show();

    }

    public boolean isBookmarked(NewsModel newsModel) {
        ArrayList<NewsModel> bookmarkedNews = getBookmarkedNews();
        for(int i=0; i<bookmarkedNews.size();i++){
            if(bookmarkedNews.get(i).getId()==newsModel.getId()){
                return true;
            }
        }
        return false;
    }
}
