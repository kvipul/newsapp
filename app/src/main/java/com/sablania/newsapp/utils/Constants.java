package com.sablania.newsapp.utils;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.sablania.newsapp.BuildConfig;

import java.util.HashMap;

public class Constants {

    public static HashMap<String, String> getHeader() {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        return headers;
    }

    public static void hideInputKeyboard(Activity activity, View view){
        //hide keyboard
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);

        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void handleVolleyError(Context context, VolleyError error, boolean showToast){
        if(BuildConfig.DEBUG) {
            if (error.networkResponse != null)
                Log.e("Volley Error Code- ", error.networkResponse.statusCode + "");
            Log.e("Volley Error Msg - ", error.toString() + " in " + context.getClass().getName());
        }
        if (error instanceof NoConnectionError) {
            if(showToast){
                Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }else if(error instanceof TimeoutError){
            if(showToast){
                Toast.makeText(context, "Connection Timeout! Please try again.", Toast.LENGTH_SHORT).show();
            }
        }else{
            if(showToast){
                Toast.makeText(context, "Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

