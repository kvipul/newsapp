package com.sablania.newsapp.utils;

import com.sablania.newsapp.models.NewsModel;

import java.util.Comparator;

/**
 * Created by ViPul Sublaniya on 17-09-2017.
 */

public class CompareNewsObject implements Comparator<NewsModel> {

    boolean newestFirst;
    public CompareNewsObject(boolean newestFirst) {
        this.newestFirst = newestFirst;
    }

    @Override
    public int compare(NewsModel lhs, NewsModel rhs) {
        String rhsId = "" + rhs.getId();
        String lhsId = "" + lhs.getId();
        if (newestFirst) {
            return rhsId.compareTo(lhsId);
        } else {
            return lhsId.compareTo(rhsId);
        }
    }
}
